import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Nomor4 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Uang Andi: ");
        int uang = Integer.parseInt(input.nextLine());
        System.out.println("Jumlah Barang: ");
        int n = Integer.parseInt(input.nextLine());

        String[] barang = new String[n];
        int[] hargaBarang = new int[n];

        for (int i = 0; i < n; i++) {
            System.out.println("Nama Barang: ");
            barang[i] = input.nextLine();
            System.out.println("Harga Barang: ");
            hargaBarang[i] = Integer.parseInt(input.nextLine());
        }

        List<Integer> barangList = new ArrayList<>();
        int i = 0;
        while (uang >= 0 && i < barang.length) {
            if (uang >= hargaBarang[i]) {
                uang -= hargaBarang[i];
                barangList.add(i);
            }
            i++;
        }

        String[] result = new String[barangList.toArray().length];
        for (i = 0; i < barangList.toArray().length; i++) {
            result[i] = barang[barangList.get(i)];
        }

        System.out.println("Hasil: ");
        for (int j = 0; j < result.length; j++) {
            System.out.print(result[j]+" ");
        }

    }
}
