import java.util.Scanner;

public class Nomor7 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("libur terakhir:");
        String [] z = input.nextLine().toLowerCase().split(" ");
        System.out.println("is kabisat?:");
        String kabisat = input.nextLine();
        System.out.println("libur mary:");
        int x = input.nextInt();
        System.out.println("libur susan:");
        int y = input.nextInt();

        String mary = "";
        String susan = "";
        boolean cek = true;

        int counter = 0;
        int hari = 0;
        while (cek){
            for (int i = 0; i < x; i++) {
                mary += "-";
            }
            mary += "l";
            for (int i = 0; i < y; i++) {
                susan += "*";
            }
            susan += "l";

            if (mary.charAt(counter) == susan.charAt(counter)){
                cek = false;
            }
            counter++;
            hari++;
        }

        if (z[1].equals("februari")){
            int tglInt = Integer.parseInt(z[0]);
            if (kabisat.equals("yes")){
                hari = hari - (28 - tglInt);
                System.out.println("tanggal "+hari+" di bulan maret");
            } else {
                if (tglInt+hari > 29){
                    hari = hari - (29 - tglInt);
                    System.out.println("tanggal "+hari+" di bulan maret");
                } else {
                    hari = hari + tglInt;
                    System.out.println("tanggal "+hari+" di bulan yg sama");
                }
            }
        } else {
            int tglInt = Integer.parseInt(z[0]);
            if (tglInt+hari > 31){
                hari = hari - (31 - tglInt);
                System.out.println("tanggal "+hari+" di bulan selanjutnya");
            } else {
                hari = hari + tglInt;
                System.out.println("tanggal "+hari+" di bulan yg sama");
            }
        }

    }
}
