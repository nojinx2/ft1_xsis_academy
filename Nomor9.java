import java.util.Scanner;

public class Nomor9 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        String [] arrayString = input.nextLine().toLowerCase().split("");
        String [] arrayKedua = input.nextLine().split(",");

        int [] arrayInt = new int[arrayString.length];
        int [] hurufInt = new int[arrayString.length];
        for (int i = 0; i < arrayInt.length; i++) {
            arrayInt[i] = Integer.parseInt(arrayKedua[i]);
        }

        for (int i = 0; i < arrayString.length; i++) {
            hurufInt[i] = (int) arrayString[i].charAt(0) - 96;
        }
        for (int i = 0; i < arrayString.length; i++) {
            if (hurufInt[i] == arrayInt[i]){
                System.out.println(arrayString[i]+" = "+arrayInt[i]+" = TRUE");
            } else {
                System.out.println(arrayString[i]+" = "+arrayInt[i]+" = FALSE");
            }
        }
    }
}
