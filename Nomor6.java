import java.util.Arrays;
import java.util.Scanner;

public class Nomor6 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Input:");
        char[] huruf = input.nextLine().toLowerCase().toCharArray();
        String vokal = "";
        String konsonan = "";
        for (int i = 0; i < huruf.length; i++) {
            if (huruf[i] == 'a' || huruf[i] == 'i' || huruf[i] == 'u' || huruf[i] == 'e' || huruf[i] == 'o'){
                vokal = vokal + huruf[i];
            } else {
                konsonan = konsonan + huruf[i];
            }
        }
        System.out.println("Huruf Vokal: "+vokal);
        konsonan = konsonan.replace(" ", "");
        System.out.println("Huruf Konsonan: "+konsonan);
    }
}
