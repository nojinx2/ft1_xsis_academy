import java.util.Scanner;

public class Nomor5 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("input:");
        String waktu = input.nextLine();

        //memisahkan string ketika bertemu : dalam string
        String[] arrayWaktu = waktu.split(":");

        int jam,menit;

        //convert string ke int
        jam = Integer.parseInt(arrayWaktu[0]);
        menit = Integer.parseInt(arrayWaktu[1].substring(0,2));
        //System.out.println(menit);
        if (jam == 12){
            jam = 0;
            System.out.println(String.format("%02d:%02d",jam,menit));
        } else if(jam < 12){
            if (arrayWaktu[1].substring(2,4) == "PM"){
                jam += 12;
                System.out.println(String.format("%02d:%02d ",jam,menit));
            } else {
                jam = jam;
                System.out.println(String.format("%02d:%02d AM",jam,menit));
            }
        } else if (jam > 12){
            jam -= 12;
            System.out.println(String.format("%02d:%02d PM",jam,menit));
        }
    }
}
